import { ApiProperty, PartialType } from '@nestjs/swagger';
import { IsDate, IsMongoId, IsNotEmpty, IsString } from 'class-validator';
import { IDocumenttype } from 'src/common/interfaces/documenttype.interface';
import { IStatus } from 'src/common/interfaces/status.interface';

export class CreateStudentDTO {
  @ApiProperty()
  @IsNotEmpty()
  @IsString()
  name: string;

  @ApiProperty()
  @IsNotEmpty()
  @IsString()
  lastname: string;

  @ApiProperty()
  @IsNotEmpty()
  @IsDate()
  birthdate: Date;

  @ApiProperty()
  @IsNotEmpty()
  @IsMongoId()
  documenttype: IDocumenttype;

  @ApiProperty()
  @IsNotEmpty()
  @IsString()
  documentnumber: string;

  @ApiProperty()
  @IsNotEmpty()
  @IsMongoId()
  status: IStatus;
}

export class UpdateStudentDTO extends PartialType(CreateStudentDTO) {}
