import { ApiProperty, PartialType } from '@nestjs/swagger';
import {
  IsDate,
  IsEmail,
  IsMongoId,
  IsNotEmpty,
  IsString,
} from 'class-validator';
import { IDocumenttype } from 'src/common/interfaces/documenttype.interface';
import { IStatus } from 'src/common/interfaces/status.interface';

export class CreateTeacherDTO {
  @ApiProperty()
  @IsNotEmpty()
  @IsString()
  name: string;

  @ApiProperty()
  @IsNotEmpty()
  @IsString()
  lastname: string;

  @ApiProperty()
  @IsNotEmpty()
  @IsDate()
  birthdate: Date;

  @ApiProperty()
  @IsNotEmpty()
  @IsMongoId()
  documenttype: IDocumenttype;

  @ApiProperty()
  @IsNotEmpty()
  @IsString()
  documentnumber: string;

  @ApiProperty()
  @IsNotEmpty()
  @IsEmail()
  email: string;

  @ApiProperty()
  @IsNotEmpty()
  @IsString()
  password: string;

  @ApiProperty()
  @IsNotEmpty()
  @IsMongoId()
  status: IStatus;
}

export class UpdateTeacherDTO extends PartialType(CreateTeacherDTO) {}
