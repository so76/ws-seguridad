import { BadRequestException, Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { IUser } from 'src/common/interfaces/user.interface';
import { USER } from 'src/common/models/models';

@Injectable()
export class UsersService {
  constructor(
    @InjectModel(USER.name)
    private readonly usersModel: Model<IUser>,
  ) {}

  async findByEmail(email: string, password: string) {
    const userPopulate = await this.usersModel
      .findOne({ email, password, isVerified: true })
      .populate({ path: 'role', select: 'name -_id' });

    if (!userPopulate) {
      throw new BadRequestException(`Usuario o contraseña incorrecta`);
    }
    const user = {
      _id: userPopulate._id,
      role: userPopulate.role.name,
      fullname: userPopulate.name + ' ' + userPopulate.lastname,
    };

    return user;
  }
}
