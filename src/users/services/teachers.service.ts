import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { IUser } from 'src/common/interfaces/user.interface';
import { USER } from 'src/common/models/models';
import { CreateTeacherDTO, UpdateTeacherDTO } from '../dtos/teacher.dto';
import { RolesService } from '../features/services/roles.service';

@Injectable()
export class TeachersService {
  constructor(
    @InjectModel(USER.name) private readonly userModel: Model<IUser>,
    private readonly rolesService: RolesService,
  ) {}

  async getTeachers(): Promise<IUser[]> {
    return await this.userModel
      .find(
        { role: Object('62c22ff6db42f2fffe3d3712') },
        { email: 0, password: 0 },
      )
      .populate({ path: 'role status', select: 'name' });
  }

  async getTeacher(id: string): Promise<IUser> {
    return await this.userModel.findOne(
      {
        _id: id,
        role: Object('62c22ff6db42f2fffe3d3712'),
      },
      { email: 0, password: 0 },
    );
  }

  async createTeacher(teacher: CreateTeacherDTO): Promise<IUser> {
    const role = await this.rolesService.getRole('62c22ff6db42f2fffe3d3712');
    return await this.userModel.create({
      ...teacher,
      role: role._id,
    });
  }

  async updateTeacher(id: string, teacher: UpdateTeacherDTO): Promise<IUser> {
    return await this.userModel.findByIdAndUpdate(id, teacher);
  }

  async deleteTeacher(id: string): Promise<IUser> {
    return await this.userModel.findByIdAndDelete(id);
  }
}
