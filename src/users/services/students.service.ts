import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { IUser } from 'src/common/interfaces/user.interface';
import { USER } from 'src/common/models/models';
import { CreateStudentDTO, UpdateStudentDTO } from '../dtos/student.dto';
import { RolesService } from '../features/services/roles.service';

@Injectable()
export class StudentsService {
  constructor(
    @InjectModel(USER.name) private readonly userModel: Model<IUser>,
    private readonly rolesService: RolesService,
  ) {}

  async getStudents(): Promise<IUser[]> {
    return await this.userModel
      .find(
        { role: Object('62c22feddb42f2fffe3d370f') },
        { email: 0, password: 0 },
      )
      .populate({ path: 'role status', select: 'name' });
  }

  async getStudent(id: string): Promise<IUser> {
    return await this.userModel.findOne(
      {
        _id: id,
        role: Object('62c22feddb42f2fffe3d370f'),
      },
      { email: 0, password: 0 },
    );
  }

  async createStudent(student: CreateStudentDTO): Promise<IUser> {
    const role = await this.rolesService.getRole('62c22feddb42f2fffe3d370f');
    return await this.userModel.create({
      ...student,
      role: role._id,
    });
  }

  async updateStudent(id: string, student: UpdateStudentDTO): Promise<IUser> {
    return await this.userModel.findByIdAndUpdate(id, student);
  }

  async deleteStudent(id: string): Promise<IUser> {
    return await this.userModel.findByIdAndDelete(id);
  }
}
