import {
  Body,
  Controller,
  Delete,
  Get,
  Param,
  Patch,
  Post,
  UseGuards,
} from '@nestjs/common';
import { ApiExcludeController } from '@nestjs/swagger';
import { Roles } from 'src/auth/decorators/roles.decorator';
import { JwtAuthGuard } from 'src/auth/guards/jwt-auth.guard';
import { RolesGuard } from 'src/auth/guards/roles.guard';
import { Role } from 'src/common/interfaces/role.interface';
import { CreateTeacherDTO, UpdateTeacherDTO } from '../dtos/teacher.dto';
import { TeachersService } from '../services/teachers.service';

@ApiExcludeController()
@UseGuards(JwtAuthGuard, RolesGuard)
@Controller('teachers')
export class TeachersController {
  constructor(private readonly teachersService: TeachersService) {}

  @Roles(Role.ADMIN, Role.DIRECTOR, Role.PROFESOR, Role.ALUMNO)
  @Get()
  getTeachers() {
    return this.teachersService.getTeachers();
  }

  @Roles(Role.ADMIN, Role.DIRECTOR, Role.PROFESOR, Role.ALUMNO)
  @Get(':id')
  getTeacher(@Param('id') id: string) {
    return this.teachersService.getTeacher(id);
  }

  @Roles(Role.ADMIN, Role.DIRECTOR, Role.PROFESOR, Role.ALUMNO)
  @Post()
  createTeacher(@Body() teacher: CreateTeacherDTO) {
    return this.teachersService.createTeacher(teacher);
  }

  @Roles(Role.ADMIN, Role.DIRECTOR, Role.PROFESOR, Role.ALUMNO)
  @Patch(':id')
  updateTeacher(@Param('id') id: string, @Body() teacher: UpdateTeacherDTO) {
    return this.teachersService.updateTeacher(id, teacher);
  }

  @Roles(Role.ADMIN, Role.DIRECTOR, Role.PROFESOR, Role.ALUMNO)
  @Delete(':id')
  deleteTeacher(@Param('id') id: string) {
    return this.teachersService.deleteTeacher(id);
  }
}
