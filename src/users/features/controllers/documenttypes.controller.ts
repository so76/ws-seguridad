import {
  Body,
  Controller,
  Delete,
  Get,
  Param,
  Patch,
  Post,
  UseGuards,
} from '@nestjs/common';
import { ApiExcludeController } from '@nestjs/swagger';
import { Roles } from 'src/auth/decorators/roles.decorator';
import { JwtAuthGuard } from 'src/auth/guards/jwt-auth.guard';
import { RolesGuard } from 'src/auth/guards/roles.guard';
import { Role } from 'src/common/interfaces/role.interface';
import {
  CreateDocumenttypeDTO,
  UpdateDocumenttypeDTO,
} from '../dtos/documenttype.dto';
import { DocumenttypesService } from '../services/documenttypes.service';

@ApiExcludeController()
@UseGuards(JwtAuthGuard, RolesGuard)
@Controller('documenttypes')
export class DocumenttypesController {
  constructor(private readonly documenttypeService: DocumenttypesService) {}

  @Roles(Role.ADMIN, Role.DIRECTOR, Role.PROFESOR, Role.ALUMNO)
  @Get()
  getDocumenttypes() {
    return this.documenttypeService.getDocumenttypes();
  }

  @Roles(Role.ADMIN, Role.DIRECTOR, Role.PROFESOR, Role.ALUMNO)
  @Get(':id')
  getDocumenttype(@Param('id') id: string) {
    return this.documenttypeService.getDocumenttype(id);
  }

  @Roles(Role.ADMIN, Role.DIRECTOR, Role.PROFESOR, Role.ALUMNO)
  @Post()
  createDocumenttype(@Body() documenttype: CreateDocumenttypeDTO) {
    return this.documenttypeService.createDocumenttype(documenttype);
  }

  @Roles(Role.ADMIN, Role.DIRECTOR, Role.PROFESOR, Role.ALUMNO)
  @Patch(':id')
  updateDocumenttype(
    @Param('id') id: string,
    @Body() documenttype: UpdateDocumenttypeDTO,
  ) {
    return this.documenttypeService.updateDocumenttype(id, documenttype);
  }

  @Roles(Role.ADMIN, Role.DIRECTOR, Role.PROFESOR, Role.ALUMNO)
  @Delete(':id')
  deleteDocumenttype(@Param('id') id: string) {
    return this.documenttypeService.deleteDocumenttype(id);
  }
}
