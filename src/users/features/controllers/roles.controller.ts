import {
  Body,
  Controller,
  Delete,
  Get,
  Param,
  Patch,
  Post,
  UseGuards,
} from '@nestjs/common';
import { ApiExcludeController } from '@nestjs/swagger';
import { Roles } from 'src/auth/decorators/roles.decorator';
import { JwtAuthGuard } from 'src/auth/guards/jwt-auth.guard';
import { RolesGuard } from 'src/auth/guards/roles.guard';
import { Role } from 'src/common/interfaces/role.interface';
import { CreateRoleDTO, UpdateRoleDTO } from '../dtos/role.dto';
import { RolesService } from '../services/roles.service';

@ApiExcludeController()
@UseGuards(JwtAuthGuard, RolesGuard)
@Controller('roles')
export class RolesController {
  constructor(private readonly rolesService: RolesService) {}

  @Roles(Role.ADMIN, Role.DIRECTOR, Role.PROFESOR, Role.ALUMNO)
  @Get()
  getRoles() {
    return this.rolesService.getRoles();
  }

  @Roles(Role.ADMIN, Role.DIRECTOR, Role.PROFESOR, Role.ALUMNO)
  @Get(':id')
  getRole(@Param('id') id: string) {
    return this.rolesService.getRole(id);
  }

  @Roles(Role.ADMIN, Role.DIRECTOR, Role.PROFESOR, Role.ALUMNO)
  @Post()
  createRole(@Body() role: CreateRoleDTO) {
    return this.rolesService.createRole(role);
  }

  @Roles(Role.ADMIN, Role.DIRECTOR, Role.PROFESOR, Role.ALUMNO)
  @Patch(':id')
  updateRole(@Param('id') id: string, @Body() role: UpdateRoleDTO) {
    return this.rolesService.updateRole(id, role);
  }

  @Roles(Role.ADMIN, Role.DIRECTOR, Role.PROFESOR, Role.ALUMNO)
  @Delete(':id')
  deleteRole(@Param('id') id: string) {
    return this.rolesService.deleteRole(id);
  }
}
