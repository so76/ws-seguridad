import { PartialType } from '@nestjs/swagger';
import { IsNotEmpty, IsString } from 'class-validator';

export class CreateDocumenttypeDTO {
  @IsNotEmpty()
  @IsString()
  name: string;
}

export class UpdateDocumenttypeDTO extends PartialType(CreateDocumenttypeDTO) {}
