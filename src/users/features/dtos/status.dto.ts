import { PartialType } from '@nestjs/swagger';
import { IsNotEmpty, IsString } from 'class-validator';

export class CreateStatusDTO {
  @IsNotEmpty()
  @IsString()
  name: string;
}

export class UpdateStatusDTO extends PartialType(CreateStatusDTO) {}
