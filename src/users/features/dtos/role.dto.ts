import { PartialType } from '@nestjs/swagger';
import { IsNotEmpty, IsString } from 'class-validator';

export class CreateRoleDTO {
  @IsNotEmpty()
  @IsString()
  name: string;
}

export class UpdateRoleDTO extends PartialType(CreateRoleDTO) {}
