import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { IDocumenttype } from 'src/common/interfaces/documenttype.interface';
import { DOCUMENTTYPE } from 'src/common/models/models';
import {
  CreateDocumenttypeDTO,
  UpdateDocumenttypeDTO,
} from '../dtos/documenttype.dto';

@Injectable()
export class DocumenttypesService {
  constructor(
    @InjectModel(DOCUMENTTYPE.name)
    private readonly documenttypeModel: Model<IDocumenttype>,
  ) {}

  async getDocumenttypes(): Promise<IDocumenttype[]> {
    return await this.documenttypeModel.find();
  }

  async getDocumenttype(id: string): Promise<IDocumenttype> {
    return await this.documenttypeModel.findById(id);
  }

  async createDocumenttype(
    documenttype: CreateDocumenttypeDTO,
  ): Promise<IDocumenttype> {
    const code = await this.getCode();
    return await this.documenttypeModel.create({
      code,
      ...documenttype,
    });
  }

  async updateDocumenttype(
    id: string,
    documenttype: UpdateDocumenttypeDTO,
  ): Promise<IDocumenttype> {
    return await this.documenttypeModel.findByIdAndUpdate(id, documenttype);
  }

  async deleteDocumenttype(id: string): Promise<IDocumenttype> {
    return await this.documenttypeModel.findByIdAndDelete(id);
  }

  async getCode(): Promise<number> {
    const code = await this.documenttypeModel.findOne().sort({ code: -1 });
    return code ? code.code + 1 : 1;
  }
}
