import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { IStatus } from 'src/common/interfaces/status.interface';
import { STATUS } from 'src/common/models/models';
import { CreateStatusDTO, UpdateStatusDTO } from '../dtos/status.dto';

@Injectable()
export class StatusesService {
  constructor(
    @InjectModel(STATUS.name)
    private readonly documenttypeModel: Model<IStatus>,
  ) {}

  async getStatuses(): Promise<IStatus[]> {
    return await this.documenttypeModel.find();
  }

  async getStatus(id: string): Promise<IStatus> {
    return await this.documenttypeModel.findById(id);
  }

  async createStatus(status: CreateStatusDTO): Promise<IStatus> {
    const code = await this.getCode();
    return await this.documenttypeModel.create({
      code,
      ...status,
    });
  }

  async updateStatus(id: string, status: UpdateStatusDTO): Promise<IStatus> {
    return await this.documenttypeModel.findByIdAndUpdate(id, status);
  }

  async deleteStatus(id: string): Promise<IStatus> {
    return await this.documenttypeModel.findByIdAndDelete(id);
  }

  async getCode(): Promise<number> {
    const code = await this.documenttypeModel.findOne().sort({ code: -1 });
    return code ? code.code + 1 : 1;
  }
}
