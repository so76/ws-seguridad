import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { IRole } from 'src/common/interfaces/role.interface';
import { ROLE } from 'src/common/models/models';
import { CreateRoleDTO, UpdateRoleDTO } from '../dtos/role.dto';

@Injectable()
export class RolesService {
  constructor(
    @InjectModel(ROLE.name) private readonly roleModel: Model<IRole>,
  ) {}

  async getRoles(): Promise<IRole[]> {
    return await this.roleModel.find();
  }

  async getRole(id: string): Promise<IRole> {
    return await this.roleModel.findById(id);
  }

  async createRole(role: CreateRoleDTO): Promise<IRole> {
    const code = await this.getCode();
    return await this.roleModel.create({
      code,
      ...role,
    });
  }

  async updateRole(id: string, role: UpdateRoleDTO): Promise<IRole> {
    return await this.roleModel.findByIdAndUpdate(id, role);
  }

  async deleteRole(id: string): Promise<IRole> {
    return await this.roleModel.findByIdAndDelete(id);
  }

  async getCode(): Promise<number> {
    const code = await this.roleModel.findOne().sort({ code: -1 });
    return code ? code.code + 1 : 1;
  }
}
