import * as mongoose from 'mongoose';

export const StatusSchema = new mongoose.Schema(
  {
    code: { type: Number, required: true, unique: true },
    name: { type: String, required: true, unique: true },
  },
  { timestamps: false, versionKey: false },
);
