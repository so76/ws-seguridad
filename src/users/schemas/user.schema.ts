import * as mongoose from 'mongoose';

export const UserSchema = new mongoose.Schema({
  name: { type: String, required: true },
  lastname: { type: String, required: true },
  birthdate: { type: Date, required: true },
  documenttype: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'documenttypes',
    required: true,
  },
  documentnumber: { type: String, required: true, unique: true },
  email: { type: String, required: false, unique: true, sparse: true },
  password: { type: String, required: false },
  role: { type: mongoose.Schema.Types.ObjectId, ref: 'roles', required: true },
  status: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'statuses',
    required: true,
  },
});
