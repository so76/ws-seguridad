import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { DOCUMENTTYPE, ROLE, STATUS, USER } from 'src/common/models/models';
import { StudentsController } from './controllers/students.controller';
import { StudentsService } from './services/students.service';
import { RolesController } from './features/controllers/roles.controller';
import { RolesService } from './features/services/roles.service';
import { DocumenttypesService } from './features/services/documenttypes.service';
import { DocumenttypesController } from './features/controllers/documenttypes.controller';
import { ConfigModule } from '@nestjs/config';
import { UserSchema } from './schemas/user.schema';
import { DocumenttypeSchema } from './schemas/documenttype.schema';
import { RoleSchema } from './schemas/role.schema';
import { StatusSchema } from './schemas/status.schema';
import { UsersController } from './controllers/users.controller';
import { UsersService } from './services/users.service';
import { StatusesController } from './features/controllers/statuses.controller';
import { StatusesService } from './features/services/statuses.service';
import { TeachersController } from './controllers/teachers.controller';
import { TeachersService } from './services/teachers.service';

@Module({
  imports: [
    ConfigModule.forRoot({
      envFilePath: ['.env'],
      isGlobal: true,
    }),
    MongooseModule.forFeatureAsync([
      {
        name: USER.name,
        useFactory: () => UserSchema,
      },
      {
        name: DOCUMENTTYPE.name,
        useFactory: () => DocumenttypeSchema,
      },
      {
        name: ROLE.name,
        useFactory: () => RoleSchema,
      },
      {
        name: STATUS.name,
        useFactory: () => StatusSchema,
      },
    ]),
  ],
  controllers: [
    StudentsController,
    RolesController,
    DocumenttypesController,
    UsersController,
    StatusesController,
    TeachersController,
  ],
  providers: [
    StudentsService,
    RolesService,
    DocumenttypesService,
    UsersService,
    StatusesService,
    TeachersService,
  ],
  exports: [UsersService],
})
export class UserModule {}
