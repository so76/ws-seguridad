import { IStudent_course } from './student_course.interface';

export interface Score {
  name: string;
  score: number;
  date: Date;
}
[];

export interface IScore {
  _id: string;
  scores: Score;
  student_course: IStudent_course;
  createdAt?: Date;
  updatedAt?: Date;
}
