export interface IStatus {
  code: number;
  name: string;
}
