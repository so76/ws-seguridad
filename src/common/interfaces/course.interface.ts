import { IClassroom } from './classroom.interface';
import { IStatus } from './status.interface';
import { IUser } from './user.interface';

export interface ICourse {
  _id: string;
  code: number;
  name: string;
  image: string;
  teacher?: IUser;
  classroom?: IClassroom;
  status: IStatus;
  createdAt?: Date;
  updatedAt?: Date;
}
