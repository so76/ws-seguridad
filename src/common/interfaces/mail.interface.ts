import { IUser } from './user.interface';

export interface IMail {
  _id?: string;
  from: IUser;
  to: IUser[];
  asunto: string;
  message: string;
}
