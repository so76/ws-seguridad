export interface IDocumenttype {
  _id: string;
  code: number;
  name: string;
  isActive: boolean;
}
