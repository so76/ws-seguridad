import { IStatus } from './status.interface';
import { IUser } from './user.interface';

export interface IClassroom {
  _id: string;
  code: string;
  students?: [IUser];
  tutor?: IUser;
  status: IStatus;
  createdAt?: Date;
  updatedAt?: Date;
}
