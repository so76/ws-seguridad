import { ICourse } from './course.interface';
import { IStatus } from './status.interface';
import { IUser } from './user.interface';

export interface IStudent_course {
  _id: string;
  student: IUser;
  course: ICourse;
  status: IStatus;
  createdAt?: Date;
  updatedAt?: Date;
}
