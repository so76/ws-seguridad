export interface IRole {
  _id: string;
  code: number;
  name: string;
  isActive: boolean;
}

export enum Role {
  ADMIN = 'Administrador',
  DIRECTOR = 'Director',
  PROFESOR = 'Profesor',
  ALUMNO = 'Alumno',
}
