export interface PayloadToken {
  role: string;
  userId: string;
}
