import { IDocumenttype } from './documenttype.interface';
import { IRole } from './role.interface';
import { IStatus } from './status.interface';

export interface IUser {
  _id?: string;
  name: string;
  lastname: string;
  birthdate: Date;
  documenttype: IDocumenttype;
  documentnumber: string;
  email?: string;
  password?: string;
  role: IRole;
  status: IStatus;
}
