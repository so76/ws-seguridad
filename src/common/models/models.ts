export const USER = { name: 'users' };
export const DOCUMENTTYPE = { name: 'documenttypes' };
export const ROLE = { name: 'roles' };
export const STATUS = { name: 'statuses' };
export const COURSE = { name: 'courses' };
export const CLASSROOM = { name: 'classrooms' };
export const STUDENT_COURSE = { name: 'students_courses' };
export const SCORES = { name: 'scores' };
