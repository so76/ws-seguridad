import { Injectable } from '@nestjs/common';
import { JwtService } from '@nestjs/jwt';
import { UsersService } from 'src/users/services/users.service';

@Injectable()
export class AuthService {
  constructor(
    private readonly userService: UsersService,
    private readonly jwtService: JwtService,
  ) {}

  async validateUser(email: string, password: string): Promise<any> {
    const user = await this.userService.findByEmail(email, password);
    if (user) {
      console.log(user);
      return user;
    }
    return null;
  }

  async signIn(user: any) {
    const payload = {
      role: user.role,
      sub: user._id,
      fullname: user.fullname,
    };
    console.log(payload);
    return { access_token: this.jwtService.sign(payload) };
  }
}
