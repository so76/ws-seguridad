import {
  Body,
  Controller,
  Delete,
  Get,
  Param,
  Patch,
  Post,
  UseGuards,
} from '@nestjs/common';
import { ApiExcludeController } from '@nestjs/swagger';
import { Roles } from 'src/auth/decorators/roles.decorator';
import { JwtAuthGuard } from 'src/auth/guards/jwt-auth.guard';
import { RolesGuard } from 'src/auth/guards/roles.guard';
import { Role } from 'src/common/interfaces/role.interface';
import {
  CreateStudent_courseDTO,
  UpdateStudent_courseDTO,
} from '../dtos/student_course.dto';
import { Students_CoursesService } from '../services/students_courses.service';

@ApiExcludeController()
@UseGuards(JwtAuthGuard, RolesGuard)
@Controller('students_courses')
export class Students_CoursesController {
  constructor(
    private readonly students_coursesService: Students_CoursesService,
  ) {}

  @Roles(Role.ADMIN, Role.DIRECTOR, Role.PROFESOR, Role.ALUMNO)
  @Get()
  getStudentsCourses() {
    return this.students_coursesService.getStudentsCourses();
  }

  @Roles(Role.ADMIN, Role.DIRECTOR, Role.PROFESOR, Role.ALUMNO)
  @Get(':id')
  getStudentCourse(@Param('id') id: string) {
    return this.students_coursesService.getStudentCourse(id);
  }

  @Roles(Role.ADMIN, Role.DIRECTOR, Role.PROFESOR, Role.ALUMNO)
  @Post()
  createStudentCourse(@Body() student_course: CreateStudent_courseDTO) {
    return this.students_coursesService.createStudentCourse(student_course);
  }

  @Roles(Role.ADMIN, Role.DIRECTOR, Role.PROFESOR, Role.ALUMNO)
  @Patch(':id')
  updateStudentCourse(
    @Param('id') id: string,
    @Body() student_course: UpdateStudent_courseDTO,
  ) {
    return this.students_coursesService.updateStudentCourse(id, student_course);
  }

  @Roles(Role.ADMIN, Role.DIRECTOR, Role.PROFESOR, Role.ALUMNO)
  @Delete(':id')
  deleteStudentCourse(@Param('id') id: string) {
    return this.students_coursesService.deleteStudentCourse(id);
  }
}
