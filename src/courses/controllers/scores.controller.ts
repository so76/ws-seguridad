import {
  Body,
  Controller,
  Delete,
  Get,
  Param,
  Patch,
  Post,
  UseGuards,
} from '@nestjs/common';
import {
  ApiBearerAuth,
  ApiExcludeEndpoint,
  ApiOperation,
  ApiTags,
} from '@nestjs/swagger';
import { Roles } from 'src/auth/decorators/roles.decorator';
import { JwtAuthGuard } from 'src/auth/guards/jwt-auth.guard';
import { RolesGuard } from 'src/auth/guards/roles.guard';
import { Role } from 'src/common/interfaces/role.interface';
import { CreateScoreDTO, UpdateScoreDTO } from '../dtos/score.dto';
import { ScoresService } from '../services/scores.service';

@ApiTags('scores')
@UseGuards(JwtAuthGuard, RolesGuard)
@Controller('scores')
export class ScoresController {
  constructor(private readonly scoresService: ScoresService) {}

  @ApiExcludeEndpoint()
  @Roles(Role.ADMIN, Role.DIRECTOR, Role.PROFESOR, Role.ALUMNO)
  @Get()
  getScores() {
    return this.scoresService.getScores();
  }

  @ApiExcludeEndpoint()
  @Roles(Role.ADMIN, Role.DIRECTOR, Role.PROFESOR, Role.ALUMNO)
  @Get(':id')
  getScore(@Param('id') id: string) {
    return this.scoresService.getScore(id);
  }

  @ApiExcludeEndpoint()
  @Roles(Role.ADMIN, Role.DIRECTOR, Role.PROFESOR, Role.ALUMNO)
  @Post()
  createScore(@Body() score: CreateScoreDTO) {
    return this.scoresService.createScore(score);
  }

  @ApiBearerAuth()
  @ApiOperation({ summary: 'Actualizar notas del estudiante' })
  @Roles(Role.ADMIN, Role.DIRECTOR, Role.PROFESOR, Role.ALUMNO)
  @Patch(':id')
  updateScore(@Param('id') id: string, @Body() score: UpdateScoreDTO) {
    return this.scoresService.updateScore(id, score);
  }

  @ApiExcludeEndpoint()
  @Roles(Role.ADMIN, Role.DIRECTOR, Role.PROFESOR, Role.ALUMNO)
  @Delete(':id')
  deleteScore(@Param('id') id: string) {
    return this.scoresService.deleteScore(id);
  }

  @ApiBearerAuth()
  @ApiOperation({ summary: 'Ver notas de los estudiantes de un curso' })
  @Roles(Role.ADMIN, Role.DIRECTOR, Role.PROFESOR, Role.ALUMNO)
  @Get('course/:id')
  getScoresByCourse(@Param('id') id: string) {
    return this.scoresService.getScoresByCourse(id);
  }
}
