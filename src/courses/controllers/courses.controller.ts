import {
  Body,
  Controller,
  Delete,
  Get,
  Param,
  Patch,
  Post,
  Req,
  UseGuards,
} from '@nestjs/common';
import { Roles } from 'src/auth/decorators/roles.decorator';
import { JwtAuthGuard } from 'src/auth/guards/jwt-auth.guard';
import { RolesGuard } from 'src/auth/guards/roles.guard';
import { Role } from 'src/common/interfaces/role.interface';
import { CreateCourseDTO, UpdateCourseDTO } from '../dtos/course.dto';
import { CoursesService } from '../services/courses.service';
import { Request } from 'express';
import { PayloadToken } from 'src/common/interfaces/token.interface';
import {
  ApiBearerAuth,
  ApiExcludeEndpoint,
  ApiOperation,
  ApiTags,
} from '@nestjs/swagger';

@ApiTags('courses')
@UseGuards(JwtAuthGuard, RolesGuard)
@Controller('courses')
export class CoursesController {
  constructor(private readonly coursesService: CoursesService) {}

  @ApiBearerAuth()
  @ApiOperation({ summary: 'Ver los cursos en los que estoy enseñando' })
  @Roles(Role.ADMIN, Role.DIRECTOR, Role.PROFESOR, Role.ALUMNO)
  @Get()
  getCourses(@Req() req: Request) {
    const user = req.user as PayloadToken;
    const userId = user.userId;
    return this.coursesService.getCourses(userId);
  }

  @ApiBearerAuth()
  @ApiOperation({ summary: 'Ver un curso en especifico' })
  @Roles(Role.ADMIN, Role.DIRECTOR, Role.PROFESOR, Role.ALUMNO)
  @Get(':id')
  getCourse(@Param('id') id: string, @Req() req: Request) {
    const user = req.user as PayloadToken;
    const userId = user.userId;
    return this.coursesService.getCourse(id, userId);
  }

  @ApiExcludeEndpoint()
  @Roles(Role.ADMIN, Role.DIRECTOR, Role.PROFESOR, Role.ALUMNO)
  @Post()
  createCourse(@Body() course: CreateCourseDTO) {
    return this.coursesService.createCourse(course);
  }

  @ApiExcludeEndpoint()
  @Roles(Role.ADMIN, Role.DIRECTOR, Role.PROFESOR, Role.ALUMNO)
  @Patch(':id')
  updateCourse(@Param('id') id: string, @Body() course: UpdateCourseDTO) {
    return this.coursesService.updateCourse(id, course);
  }

  @ApiExcludeEndpoint()
  @Roles(Role.ADMIN, Role.DIRECTOR, Role.PROFESOR, Role.ALUMNO)
  @Delete(':id')
  deleteCourse(@Param('id') id: string) {
    return this.coursesService.deleteCourse(id);
  }
}
