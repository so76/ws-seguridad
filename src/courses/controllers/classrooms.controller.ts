import {
  Body,
  Controller,
  Delete,
  Get,
  Param,
  Patch,
  Post,
} from '@nestjs/common';
import { ApiExcludeController } from '@nestjs/swagger';
import { Roles } from 'src/auth/decorators/roles.decorator';
import { Role } from 'src/common/interfaces/role.interface';
import { CreateClassroomDTO, UpdateClassroomDTO } from '../dtos/classroom.dto';
import { ClassroomsService } from '../services/classrooms.service';

@ApiExcludeController()
@Controller('classrooms')
export class ClassroomsController {
  constructor(private readonly classroomsService: ClassroomsService) {}

  @Roles(Role.ADMIN, Role.DIRECTOR, Role.PROFESOR, Role.ALUMNO)
  @Get()
  getClassrooms() {
    return this.classroomsService.getClassrooms();
  }

  @Roles(Role.ADMIN, Role.DIRECTOR, Role.PROFESOR, Role.ALUMNO)
  @Get(':id')
  getClassroom(@Param('id') id: string) {
    return this.classroomsService.getClassroom(id);
  }

  @Roles(Role.ADMIN, Role.DIRECTOR, Role.PROFESOR, Role.ALUMNO)
  @Post()
  createClassroom(@Body() classroom: CreateClassroomDTO) {
    return this.classroomsService.createClassroom(classroom);
  }

  @Roles(Role.ADMIN, Role.DIRECTOR, Role.PROFESOR, Role.ALUMNO)
  @Patch(':id')
  updateClassroom(
    @Param('id') id: string,
    @Body() classroom: UpdateClassroomDTO,
  ) {
    return this.classroomsService.updateClassroom(id, classroom);
  }

  @Roles(Role.ADMIN, Role.DIRECTOR, Role.PROFESOR, Role.ALUMNO)
  @Delete(':id')
  deleteClassroom(@Param('id') id: string) {
    return this.classroomsService.deleteClassroom(id);
  }
}
