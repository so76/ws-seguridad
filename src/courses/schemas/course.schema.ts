import * as mongoose from 'mongoose';

export const CourseSchema = new mongoose.Schema(
  {
    code: { type: Number, required: true, unique: true },
    name: { type: String, required: true },
    image: { type: String, required: true },
    teacher: {
      type: mongoose.Schema.Types.ObjectId,
      ref: 'users',
      required: false,
    },
    classroom: {
      type: mongoose.Schema.Types.ObjectId,
      ref: 'classrooms',
      required: false,
    },
    status: {
      type: mongoose.Schema.Types.ObjectId,
      ref: 'statuses',
      required: true,
    },
  },
  { timestamps: true, versionKey: false },
);
