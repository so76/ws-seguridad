import * as mongoose from 'mongoose';

export const ClassroomSchema = new mongoose.Schema(
  {
    code: { type: String, required: true, unique: true },
    students: [
      {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'users',
        required: false,
      },
    ],
    tutor: {
      type: mongoose.Schema.Types.ObjectId,
      ref: 'users',
      required: false,
    },
    status: {
      type: mongoose.Schema.Types.ObjectId,
      ref: 'statuses',
      required: true,
    },
  },
  { timestamps: true, versionKey: false },
);
