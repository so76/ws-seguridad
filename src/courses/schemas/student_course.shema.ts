import * as mongoose from 'mongoose';

export const Student_courseSchema = new mongoose.Schema(
  {
    student: {
      type: mongoose.Schema.Types.ObjectId,
      ref: 'users',
      required: false,
    },
    course: {
      type: mongoose.Schema.Types.ObjectId,
      ref: 'courses',
      required: false,
    },
    status: {
      type: mongoose.Schema.Types.ObjectId,
      ref: 'statuses',
      required: true,
    },
  },
  { timestamps: true, versionKey: false },
);
