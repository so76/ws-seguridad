import * as mongoose from 'mongoose';

export const ScoresSchema = new mongoose.Schema(
  {
    scores: [
      {
        _id: { type: mongoose.Schema.Types.ObjectId },
        name: { type: String, required: true },
        score: { type: Number, required: true },
        date: { type: Date, required: true, default: Date.now },
      },
    ],
    student_course: {
      type: mongoose.Schema.Types.ObjectId,
      ref: 'students_courses',
      required: true,
      unique: true,
    },
  },
  { timestamps: true, versionKey: false },
);
