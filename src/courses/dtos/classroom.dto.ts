import { ApiProperty, PartialType } from '@nestjs/swagger';
import {
  IsArray,
  IsMongoId,
  IsNotEmpty,
  IsOptional,
  IsString,
} from 'class-validator';
import { IStatus } from 'src/common/interfaces/status.interface';
import { IUser } from 'src/common/interfaces/user.interface';

export class CreateClassroomDTO {
  @ApiProperty()
  @IsNotEmpty()
  @IsString()
  code: string;

  @ApiProperty()
  @IsOptional()
  @IsArray()
  students: [IUser];

  @ApiProperty()
  @IsOptional()
  @IsMongoId()
  tutor: IUser;

  @ApiProperty()
  @IsNotEmpty()
  @IsMongoId()
  status: IStatus;
}

export class UpdateClassroomDTO extends PartialType(CreateClassroomDTO) {}
