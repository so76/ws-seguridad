import { ApiProperty, PartialType } from '@nestjs/swagger';
import { IsMongoId, IsNotEmpty } from 'class-validator';
import { ICourse } from 'src/common/interfaces/course.interface';
import { IStatus } from 'src/common/interfaces/status.interface';
import { IUser } from 'src/common/interfaces/user.interface';

export class CreateStudent_courseDTO {
  @ApiProperty()
  @IsNotEmpty()
  @IsMongoId()
  student: IUser;

  @ApiProperty()
  @IsNotEmpty()
  @IsMongoId()
  course: ICourse;

  @ApiProperty()
  @IsNotEmpty()
  @IsMongoId()
  status: IStatus;
}

export class UpdateStudent_courseDTO extends PartialType(
  CreateStudent_courseDTO,
) {}
