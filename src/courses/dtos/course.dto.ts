import { ApiProperty, PartialType } from '@nestjs/swagger';
import {
  IsMongoId,
  IsNotEmpty,
  IsNumber,
  IsOptional,
  IsString,
} from 'class-validator';
import { IClassroom } from 'src/common/interfaces/classroom.interface';
import { IStatus } from 'src/common/interfaces/status.interface';
import { IUser } from 'src/common/interfaces/user.interface';

export class CreateCourseDTO {
  @ApiProperty()
  @IsNotEmpty()
  @IsNumber()
  code: number;

  @ApiProperty()
  @IsNotEmpty()
  @IsString()
  name: string;

  @ApiProperty()
  @IsNotEmpty()
  @IsString()
  image: string;

  @ApiProperty()
  @IsOptional()
  @IsMongoId()
  teacher: IUser;

  @ApiProperty()
  @IsOptional()
  @IsMongoId()
  classroom: IClassroom;

  @ApiProperty()
  @IsNotEmpty()
  @IsMongoId()
  status: IStatus;
}

export class UpdateCourseDTO extends PartialType(CreateCourseDTO) {}
