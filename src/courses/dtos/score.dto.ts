import { ApiProperty, PartialType } from '@nestjs/swagger';
import {
  IsArray,
  IsDate,
  IsMongoId,
  IsNotEmpty,
  IsNumber,
  IsString,
  Max,
  Min,
  ValidateNested,
} from 'class-validator';
import { IStudent_course } from 'src/common/interfaces/student_course.interface';

export class Scores {
  @ApiProperty({ description: 'The name of qualified practice' })
  @IsNotEmpty()
  @IsString()
  name: string;

  @ApiProperty({ description: 'The score from 0 to 20 of the student ' })
  @IsNotEmpty()
  @IsNumber()
  @Min(0)
  @Max(20)
  score: number;

  @ApiProperty({ description: 'The date of the score comes by default' })
  @IsNotEmpty()
  @IsDate()
  date: Date = new Date(Date.now());
}

export class CreateScoreDTO {
  @ApiProperty({ type: [Scores] })
  @IsNotEmpty()
  @IsArray()
  @ValidateNested({ each: true })
  scores: Scores;

  @ApiProperty({ description: 'The id of existing student_course' })
  @IsNotEmpty()
  @IsMongoId()
  student_course: IStudent_course;
}

export class UpdateScoreDTO extends PartialType(CreateScoreDTO) {}
