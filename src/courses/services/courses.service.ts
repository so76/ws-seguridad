import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { ICourse } from 'src/common/interfaces/course.interface';
import { COURSE } from 'src/common/models/models';
import { CreateCourseDTO, UpdateCourseDTO } from '../dtos/course.dto';

@Injectable()
export class CoursesService {
  constructor(
    @InjectModel(COURSE.name) private readonly courseModel: Model<ICourse>,
  ) {}

  async getCourses(userId: string): Promise<ICourse[]> {
    return await this.courseModel
      .find(
        { teacher: Object(userId) },
        { teacher: 0, classroom: 0, status: 0, createdAt: 0, updatedAt: 0 },
      )
      .populate({ path: 'teacher status', select: 'name' })
      .populate({ path: 'classroom', select: 'code' });
  }

  async getCourse(id: string, userId: string): Promise<ICourse> {
    return await this.courseModel
      .findOne(
        { _id: id, teacher: Object(userId) },
        { createdAt: 0, updatedAt: 0 },
      )
      .populate({ path: 'teacher status', select: 'name' })
      .populate({ path: 'classroom', select: 'code' });
  }

  async createCourse(course: CreateCourseDTO): Promise<ICourse> {
    return await this.courseModel.create(course);
  }

  async updateCourse(id: string, course: UpdateCourseDTO): Promise<ICourse> {
    return await this.courseModel.findByIdAndUpdate(id, course);
  }

  async deleteCourse(id: string): Promise<ICourse> {
    return await this.courseModel.findByIdAndDelete(id);
  }
}
