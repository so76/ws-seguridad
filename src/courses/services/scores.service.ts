import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { IScore } from 'src/common/interfaces/score.interface';
import { SCORES } from 'src/common/models/models';
import { CreateScoreDTO, UpdateScoreDTO } from '../dtos/score.dto';

@Injectable()
export class ScoresService {
  constructor(
    @InjectModel(SCORES.name)
    private readonly scoresModel: Model<IScore>,
  ) {}

  async getScores(): Promise<IScore[]> {
    return this.scoresModel.find({}, { createdAt: 0, updatedAt: 0 }).populate({
      path: 'student_course',
      select: 'student course',
      populate: { path: 'student course', select: 'name lastname' },
    });
  }

  async getScore(id: string): Promise<IScore> {
    return this.scoresModel.findById(id);
  }

  async createScore(score: CreateScoreDTO): Promise<IScore> {
    return this.scoresModel.create(score);
  }

  async updateScore(id: string, score: UpdateScoreDTO): Promise<IScore> {
    return this.scoresModel.findByIdAndUpdate(id, score, { new: true });
  }

  async deleteScore(id: string): Promise<IScore> {
    return this.scoresModel.findByIdAndDelete(id);
  }

  async getScoresByCourse(id: string): Promise<IScore[]> {
    const scores = await this.scoresModel
      .find({}, { createdAt: 0, updatedAt: 0 })
      .populate({
        path: 'student_course',
        match: { course: id },
        select: 'student course',
        populate: { path: 'student course', select: 'name lastname' },
      });

    return scores.filter((score) => score.student_course !== null);
  }
}
