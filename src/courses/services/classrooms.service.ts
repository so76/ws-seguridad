import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { IClassroom } from 'src/common/interfaces/classroom.interface';
import { CLASSROOM } from 'src/common/models/models';
import { CreateClassroomDTO, UpdateClassroomDTO } from '../dtos/classroom.dto';

@Injectable()
export class ClassroomsService {
  constructor(
    @InjectModel(CLASSROOM.name)
    private readonly classroomModel: Model<IClassroom>,
  ) {}

  async getClassrooms(): Promise<IClassroom[]> {
    return await this.classroomModel
      .find()
      .populate({ path: 'students tutor status', select: 'name' });
  }

  async getClassroom(id: string): Promise<IClassroom> {
    return await this.classroomModel
      .findById(id)
      .populate({ path: 'students tutor status', select: 'name' });
  }

  async createClassroom(classroom: CreateClassroomDTO): Promise<IClassroom> {
    return await this.classroomModel.create(classroom);
  }

  async updateClassroom(
    id: string,
    classroom: UpdateClassroomDTO,
  ): Promise<IClassroom> {
    return await this.classroomModel.findByIdAndUpdate(id, classroom);
  }

  async deleteClassroom(id: string): Promise<IClassroom> {
    return await this.classroomModel.findByIdAndDelete(id);
  }
}
