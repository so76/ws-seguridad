import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { IStudent_course } from 'src/common/interfaces/student_course.interface';
import { STUDENT_COURSE } from 'src/common/models/models';
import {
  CreateStudent_courseDTO,
  UpdateStudent_courseDTO,
} from '../dtos/student_course.dto';

@Injectable()
export class Students_CoursesService {
  constructor(
    @InjectModel(STUDENT_COURSE.name)
    private readonly students_coursesModel: Model<IStudent_course>,
  ) {}

  async getStudentsCourses(): Promise<IStudent_course[]> {
    return await this.students_coursesModel.find().populate({
      path: 'student course status',
      select: 'name',
    });
  }

  async getStudentCourse(id: string): Promise<IStudent_course> {
    return await this.students_coursesModel.findById(id).populate({
      path: 'student course status',
      select: 'name',
    });
  }

  async createStudentCourse(
    student_course: CreateStudent_courseDTO,
  ): Promise<IStudent_course> {
    return await this.students_coursesModel.create(student_course);
  }

  async updateStudentCourse(
    id: string,
    student_course: UpdateStudent_courseDTO,
  ): Promise<IStudent_course> {
    return await this.students_coursesModel.findByIdAndUpdate(
      id,
      student_course,
    );
  }

  async deleteStudentCourse(id: string): Promise<IStudent_course> {
    return await this.students_coursesModel.findByIdAndDelete(id);
  }
}
