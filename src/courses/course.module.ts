import { Module } from '@nestjs/common';
import { ConfigModule } from '@nestjs/config';
import { MongooseModule } from '@nestjs/mongoose';
import {
  CLASSROOM,
  COURSE,
  SCORES,
  STUDENT_COURSE,
} from 'src/common/models/models';
import { CoursesController } from './controllers/courses.controller';
import { CourseSchema } from './schemas/course.schema';
import { CoursesService } from './services/courses.service';
import { ClassroomsController } from './controllers/classrooms.controller';
import { ClassroomsService } from './services/classrooms.service';
import { ClassroomSchema } from './schemas/classroom.schema';
import { Students_CoursesController } from './controllers/students_courses.controller';
import { Students_CoursesService } from './services/students_courses.service';
import { Student_courseSchema } from './schemas/student_course.shema';
import { ScoresSchema } from './schemas/scores.schema';
import { ScoresController } from './controllers/scores.controller';
import { ScoresService } from './services/scores.service';

@Module({
  imports: [
    ConfigModule.forRoot({
      envFilePath: ['.env'],
      isGlobal: true,
    }),
    MongooseModule.forFeatureAsync([
      {
        name: COURSE.name,
        useFactory: () => CourseSchema,
      },
      {
        name: CLASSROOM.name,
        useFactory: () => ClassroomSchema,
      },
      {
        name: STUDENT_COURSE.name,
        useFactory: () => Student_courseSchema,
      },
      {
        name: SCORES.name,
        useFactory: () => ScoresSchema,
      },
    ]),
  ],
  controllers: [
    CoursesController,
    ClassroomsController,
    Students_CoursesController,
    ScoresController,
  ],
  providers: [
    CoursesService,
    ClassroomsService,
    Students_CoursesService,
    ScoresService,
  ],
})
export class CourseModule {}
